<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface ObserverInterface
{
  public function getStore();
  public function getEvent();
  public function getCreditmemo();
}
