<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\ViewInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Event\ManagerInterface as EventManagerInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Profiler;
use Magento\Framework\UrlInterface;

abstract class AbstractActionController implements ActionInterface
{
  /**
   * @var ?RequestInterface
   */
  protected ?RequestInterface $_request;

  /**
   * @var ?ResponseInterface
   */
  protected ?ResponseInterface $_response;

  /**
   * @var ?RedirectFactory
   */
  protected ?RedirectFactory $resultRedirectFactory;

  /**
   * @var ?ResultFactory
   */
  protected ?ResultFactory $resultFactory;
  /**
   * @var ?ObjectManagerInterface
   */
  protected ?ObjectManagerInterface $_objectManager;

  /**
   * Namespace for session.
   * Should be defined for proper working session.
   */
  protected string $_sessionNamespace;

  /**
   * @var ?EventManagerInterface
   */
  protected ?EventManagerInterface $_eventManager;

  /**
   * @var ?ActionFlag
   */
  protected ?ActionFlag $_actionFlag;

  /**
   * @var ?RedirectInterface
   */
  protected ?RedirectInterface $_redirect;

  /**
   * @var ?ViewInterface
   */
  protected ?ViewInterface $_view;

  /**
   * @var ?UrlInterface
   */
  protected ?UrlInterface $_url;

  /**
   * @var ?MessageManagerInterface
   */
  protected ?MessageManagerInterface $messageManager;

  public function __construct(Context $context)
  {
    $this->_objectManager = $context->getObjectManager();
    $this->_eventManager = $context->getEventManager();
    $this->_url = $context->getUrl();
    $this->_actionFlag = $context->getActionFlag();
    $this->_redirect = $context->getRedirect();
    $this->_view = $context->getView();
    $this->messageManager = $context->getMessageManager();

    $this->_request = $context->getRequest();
    $this->_response = $context->getResponse();
    $this->resultFactory = $context->getResultFactory();
    $this->resultRedirectFactory = $context->getResultRedirectFactory();
  }

  /**
   * Dispatch request.
   *
   * @param  RequestInterface  $request
   *
   * @return ResponseInterface
   *
   * @throws NotFoundException
   */
  public function dispatch(RequestInterface $request): ResponseInterface
  {
    $this->_request = $request;
    $profilerKey = 'CONTROLLER_ACTION:'.$request->getFullActionName();
    Profiler::start($profilerKey);

    $result = null;
    if ($request->isDispatched() && !$this->_actionFlag->get('', self::FLAG_NO_DISPATCH)) {
      Profiler::start('action_body');
      $result = $this->execute();
      Profiler::stop('action_body');
    }
    Profiler::stop($profilerKey);

    return $result ? $result : $this->_response;
  }

  /**
   * Returns ActionFlag value.
   */
  public function getActionFlag(): ActionFlag
  {
    return $this->_actionFlag;
  }

  /**
   * Retrieve request object.
   */
  public function getRequest(): RequestInterface
  {
    return $this->_request;
  }

  /**
   * Retrieve response object.
   *
   * @return ResponseInterface
   */
  public function getResponse(): ?ResponseInterface
  {
    return $this->_response;
  }

  /**
   * Throw control to different action (control and module if was specified).
   *
   * @param  string  $action
   * @param  null  $controller
   * @param  null  $module
   * @param  array|null  $params
   *
   * @return ResponseInterface
   */
  protected function _forward(
      string $action,
      $controller = null,
      $module = null,
      ?array $params = null
  ): ?ResponseInterface {
    $request = $this->getRequest();

    $request->initForward();

    if (isset($params)) {
      $request->setParams($params);
    }

    if (isset($controller)) {
      $request->setControllerName($controller);

      // Module should only be reset if controller has been specified
      if (isset($module)) {
        $request->setModuleName($module);
      }
    }

    $request->setActionName($action);
    $request->setDispatched(false);

    return $this->getResponse();
  }

  /**
   * Set redirect into response.
   *
   * @param  string  $path
   * @param  array  $arguments
   *
   * @return ResponseInterface
   */
  protected function _redirect(string $path, $arguments = []): ResponseInterface
  {
    $this->_redirect->redirect($this->getResponse(), $path, $arguments);

    return $this->getResponse();
  }

  protected function getRedirectErrorResponse($url, array $params = []): array
  {
    return $this->getRedirectResponse(false, $url, $params);
  }

  protected function getRedirectResponse($valid, $url, array $params = []): array
  {
    return array_merge(
        $params,
        [
            'error' => $valid ? 0 : 1,
            'success' => $valid ? 1 : 0,
            'message' => '',
            'redirecturl' => $url,
        ]
    );
  }
}
