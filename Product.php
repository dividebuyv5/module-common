<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Catalog\Model\Product as MagentoProduct;

abstract class Product extends MagentoProduct implements ProductInterface
{
}
