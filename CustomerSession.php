<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Customer\Model\Session;

abstract class CustomerSession extends Session implements CheckoutSessionInterface
{
  abstract public function setCartWasUpdated($status);
  abstract public function setIsNewShipment($isShipment);
  abstract public function getIsNewShipment();
  abstract public function unsIsNewShipment();
}
