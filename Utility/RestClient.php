<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class RestClient
{
  private ClientInterface $httpClient;

  public function __construct(Client $httpClient)
  {
    $this->httpClient = $httpClient;
  }

  /**
   * @param $url
   * @param  mixed  $params
   * @param  string  $method
   *
   * @return ResponseInterface
   *
   * @throws GuzzleException
   */
  public function request($url, $params, $method = 'POST'): ResponseInterface
  {
    $options = [
        'verify' => false,
      //'debug' => str_contains($url, 'sandbox'),
        'http_errors' => false,
    ];
    if (strtoupper($method) === 'GET') {
      $options['query'] = $params;
    } else {
      if (is_string($params)) {
        $options['body'] = $params;
      } else {
        $options['json'] = $params;
      }
      $options['headers'] = [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
      ];
    }

    return $this->httpClient->request($method, $url, $options);
  }
}
