<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Dividebuy\Common\Constants\DivideBuy;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order as OrderModel;

class ProductHelper
{
  private ProductFactory $productLoader;
  private StoreConfigHelper $storeConfig;
  private CurrencyFactory $currencyFactory;
  private OrderModel $orderRepository;
  private ProductRepositoryInterface $productRepository;
  private Product $productModel;
  private PriceCurrencyInterface $priceCurrency;

  public function __construct(
      ProductFactory $productLoader,
      StoreConfigHelper $configHelper,
      CurrencyFactory $currencyFactory,
      OrderModel $orderRepository,
      ProductRepositoryInterface $productRepository,
      Product $productModel,
      PriceCurrencyInterface $priceCurrency
  ) {
    $this->productLoader = $productLoader;
    $this->storeConfig = $configHelper;
    $this->currencyFactory = $currencyFactory;
    $this->productModel = $productModel;
    $this->orderRepository = $orderRepository;
    $this->productRepository = $productRepository;
    $this->priceCurrency = $priceCurrency;
  }

  public function getSymbol(): string
  {
    $currentCurrency = $this->getStoreConfig()->getStore()->getCurrentCurrencyCode();
    $currency = $this->currencyFactory->create()->load($currentCurrency);

    return (string) $currency->getCurrencySymbol();
  }

  public function getStoreConfig(): StoreConfigHelper
  {
    return $this->storeConfig;
  }

  public function getProductLoader(): ProductFactory
  {
    return $this->productLoader;
  }

  public function loadProduct($productId, $storeId = null)
  {
    if ($storeId) {
      return $this->productModel->setStoreId($storeId)->load($productId);
    }

    return $this->productModel->load($productId);
  }

  public function createPosProduct($websiteId)
  {
    $this->productModel->setSku(DivideBuy::DIVIDEBUY_POS_PRODUCT);
    $this->productModel->setName('Dividebuy Custom Order Product');
    $this->productModel->setWebsiteId($websiteId);
    $this->productModel->setAttributeSetId(4);
    $this->productModel->setStatus(1);
    $this->productModel->setVisibility(1);
    $this->productModel->setTaxClassId(0);
    $this->productModel->setTypeId('simple');
    $this->productModel->setPrice(1);
    $this->productModel->setStockData(
        [
            'use_config_manage_stock' => 0,
            'manage_stock' => 0,
            'is_in_stock' => 1,
        ]
    );
    $this->productModel->save();

    return $this->productModel->getId();
  }

  public function getProductBySku($sku)
  {
    return $this->productRepository->get($sku);
  }

  public function formatPrice(
      $amount,
      $includeContainer = true,
      $precision = PriceCurrencyInterface::DEFAULT_PRECISION,
      $scope = null,
      $currency = null
  ): string {
    return (string) $this->priceCurrency->format($amount, $includeContainer, $precision, $scope, $currency);
  }
}
