<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Dividebuy\Common\CheckoutSessionInterface;
use Dividebuy\Common\Registry;
use Dividebuy\Common\CustomerInterface;
use Magento\Backend\Model\Session as BackendSession;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class SessionHelper
{
  /**
   * @var CheckoutSession | CheckoutSessionInterface
   */
  private CheckoutSession $checkoutSession;
  private CustomerSession $customerSession;
  private StoreConfigHelper $storeConfigHelper;
  private ObjectManager $objectManager;
  private BackendSession $backendSession;
  private Cart $cart;
  private Registry $registry;

  public function __construct(
      CheckoutSession $checkoutSession,
      CustomerSession $customerSession,
      Cart $cart,
      StoreConfigHelper $storeConfigHelper,
      BackendSession $backendSession,
      Registry $registry
  ) {
    $this->cart = $cart;
    $this->checkoutSession = $checkoutSession;
    $this->customerSession = $customerSession;
    $this->storeConfigHelper = $storeConfigHelper;
    $this->backendSession = $backendSession;
    $this->registry = $registry;
    $this->objectManager = ObjectManager::getInstance();
  }

  public function getCheckoutSession()
  {
    return $this->checkoutSession;
  }

  public function addSessionProducts()
  {
    //Getting value of current session for cart products.
    $sessionArray = $this->getCheckoutSession()->getTemparoryCart();
    $this->checkoutSession->unsTemparoryCart();

    foreach ($sessionArray as $item) {
      $productType = $item['product_type'];
      $itemInfoRequest = (new JsonHelper())->unserialize($item['info_byRequest']);

      // Checking product types and based on that adding respected item values to cart.
      if ($productType === 'simple') {
        $simpleItemParams = [];
        if (!empty($itemInfoRequest['options'])) {
          $simpleItemParams['options'] = $itemInfoRequest['options'];
        }
        $simpleItemParams['qty'] = $item['qty'];
        $this->addProductsInCart($item['product_id'], $simpleItemParams);
      }
      if ($productType === 'bundle') {
        $bundleItemParams = [];
        $bundleItemParams['bundle_option'] = $itemInfoRequest['bundle_option'];
        $bundleItemParams['bundle_option_qty'] = $itemInfoRequest['bundle_option_qty'];
        $bundleItemParams['qty'] = $itemInfoRequest['qty'];
        $this->addProductsInCart($item['product_id'], $bundleItemParams);
      }
      if ($productType === 'configurable') {
        $configurableItemParams = [];
        $configurableItemParams['super_attribute'] = $itemInfoRequest['super_attribute'];
        $configurableItemParams['qty'] = $itemInfoRequest['qty'];
        $this->addProductsInCart($item['product_id'], $configurableItemParams);
      }
      if ($productType === 'grouped') {
        $groupedItemParams = [];
        $groupedItemParams['super_product_config'] = $itemInfoRequest['super_product_config'];
        $groupedItemParams['qty'] = $item['qty'];
        $this->addProductsInCart($item['product_id'], $groupedItemParams);
      }
      if ($productType === 'downloadable') {
        $downloadableItemParams = [];
        $downloadableItemParams['qty'] = $itemInfoRequest['qty'];
        $request = new DataObject();
        $request->setData($downloadableItemParams);
        $this->addProductsInCart($item['product_id'], $request);
      }
      unset($simpleItemParams, $bundleItemParams, $configurableItemParams, $groupedItemParams);
    }
  }

  public function setInstallment($installment)
  {
    return $this->checkoutSession->setSelectedInstalment($installment);
  }

  public function isLoggedIn(): bool
  {
    return (bool) $this->customerSession->isLoggedIn();
  }

  /**
   * @return Customer | CustomerInterface
   */
  public function getCustomer(): Customer
  {
    return $this->customerSession->getCustomer();
  }

  public function setUserEmail($userEmail)
  {
    return $this->checkoutSession->setDividebuyUserEmail($userEmail);
  }

  public function getUserEmail()
  {
    return $this->checkoutSession->getDividebuyUserEmail();
  }

  public function getLastOrderId()
  {
    return $this->checkoutSession->getLastOrderId();
  }

  public function getPhoneOrderToken()
  {
    return $this->checkoutSession->getDividebuyPhoneOrderToken();
  }

  public function unsetCheckoutSessionAndToken()
  {
    $this->checkoutSession->unsDividebuyPhoneOrderToken();
    $this->checkoutSession->unsDividebuyCheckoutSession();
  }

  public function hasTemporaryCart(): bool
  {
    $cart = $this->checkoutSession->getTemparoryCart();

    return !empty($cart);
  }

  public function restoreQuote()
  {
    return $this->checkoutSession->restoreQuote();
  }

  public function getQuote()
  {
    return $this->checkoutSession->getQuote();
  }

  /**
   * Used to add product in cart.
   *
   * @param mixed   $productId
   * @param mixed $itemParams
   *
   * @throws LocalizedException
   */
  public function addProductsInCart($productId, $itemParams)
  {
    $storeId = $this->getStoreConfigHelper()->getStoreId();
    $product = $this->objectManager->create(Product::class)->setStoreId($storeId)->load($productId);

    $this->cart->addProduct($product, $itemParams);
    $this->customerSession->setCartWasUpdated(true);
    $this->cart->save();
  }

  public function getStoreConfigHelper(): StoreConfigHelper
  {
    return $this->storeConfigHelper;
  }

  public function getBackendSession(): BackendSession
  {
    return $this->backendSession;
  }

  public function getRegistry(): Registry
  {
    return $this->registry;
  }
}
