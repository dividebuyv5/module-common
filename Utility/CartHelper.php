<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Dividebuy\Common\Logger\Logger;
use Dividebuy\Common\Quote;
use Magento\Checkout\Model\Cart as CartModel;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Model\Order;

class CartHelper
{
  private CartModel $cartModel;
  private Item $quoteItem;
  private Order $orderModel;
  private ProductHelper $productHelper;
  private QuoteFactory $quoteLoader;
  private SessionHelper $sessionHelper;
  private UrlInterface $urlBuilder;
  private CartRepositoryInterface $cartRepo;
  private CartManagementInterface $cartManager;

  /**
   * Contains the details of current cart items.
   */
  private array $cartSplit = [];

  /**
   * Contains the details of all dividebuy products of current cart.
   */
  private array $divideBuy = [];

  /**
   * Contains the details of all non dividebuy products of current cart.
   */
  private array $nonDivideBuy = [];

  /**
   * Contains the ids of all non dividebuy products of current cart.
   */
  private array $removeIds = [];

  /**
   * Contains the ids of all dividebuy products of current cart.
   */
  private array $divideBuyIds = [];

  /**
   * Contains all dividebuy products detail.
   */
  private array $divideBuyItems = [];
  /**
   * Contains all non dividebuy products detail.
   */
  private array $otherItems = [];

  /**
     * Layout
     *
     * @var \Magento\Framework\View\LayoutInterface
     */
     protected $_layout;

  public function __construct(
      CartModel $cartModel,
      Item $quoteItem,
      Order $orderModel,
      SessionHelper $sessionHelper,
      ProductHelper $productHelper,
      UrlInterface $urlBuilder,
      QuoteFactory $quoteLoader,
      CartRepositoryInterface $cartRepo,
      CartManagementInterface $cartManager,
      \Magento\Framework\View\LayoutInterface $layout
  ) {
    $this->cartModel = $cartModel;
    $this->quoteItem = $quoteItem;
    $this->orderModel = $orderModel;
    $this->productHelper = $productHelper;
    $this->sessionHelper = $sessionHelper;
    $this->quoteLoader = $quoteLoader;
    $this->urlBuilder = $urlBuilder;
    $this->cartManager = $cartManager;
    $this->cartRepo = $cartRepo;
    $this->_layout = $layout;
  }

  public function truncateCart(): CartModel
  {
    $cartObject = $this->cartModel->truncate();
    $cartObject->saveQuote();

    return $cartObject;
  }

  public function removeItemsById(array $divideBuyIds)
  {
    foreach ($divideBuyIds as $id) {
      $quoteItem = $this->quoteItem->load($id);
      $quoteItem->delete();
    }
  }

  public function getOrderById($orderId)
  {
    return $this->orderModel->load($orderId);
  }

  public function getCartUrl(): string
  {
    return (string) $this->urlBuilder->getUrl('checkout/cart', ['_secure' => true]);
  }

  public function getCartItems()
  {
    return $this->getItemArray();
  }

  public function getSoftSearchCartItems()
  {
    return $this->getSoftSearchItemArray();
  }

  /**
   * Used to get array of cart items with the count of divide buy products only.
   *
   * @param  null  $quoteId
   *
   * @return array|string
   */
  public function getSoftSearchItemArray($quoteId = null)
  {
    if ($this->cartSplit) {
      return $this->cartSplit;
    }
    $carts = $this->sessionHelper->getQuote()->getAllItems();

    if (empty($carts)) {
      if ($quoteId) {
        $quote = $this->loadQuote($quoteId);
        $carts = $quote->getAllItems();
      } else {
        return 'no-items';
      }
    }

    $itemCount = 0;
    foreach ($carts as $item) {
      if ($item->getParentItemId()) {
        continue;
      }
      ++$itemCount;
      $product = $this->loadProduct($item->getProductId());

      if ($product->getDividebuyEnable()) {
        $this->divideBuy[] = $item;
        $this->divideBuyIds[] = $item->getId();
      }
    }

    return $this->itemArray($itemCount);
  }


  /**
   * Used to get array of cart items with the count of divide buy and non-divide buy product.
   *
   * @param  null  $quoteId
   *
   * @return array|string
   */
  public function getItemArray($quoteId = null)
  {
    if ($this->cartSplit) {
      return $this->cartSplit;
    }
    $carts = $this->sessionHelper->getQuote()->getAllItems();

    if (empty($carts)) {
      if ($quoteId) {
        $quote = $this->loadQuote($quoteId);
        $carts = $quote->getAllItems();
      } else {
        return 'no-items';
      }
    }

    $itemCount = 0;
    foreach ($carts as $item) {
      if ($item->getParentItemId()) {
        continue;
      }
      ++$itemCount;
      $product = $this->loadProduct($item->getProductId());

      if ($product->getDividebuyEnable()) {
        $this->divideBuy[] = $item;
        $this->divideBuyIds[] = $item->getId();
      } else {
        $this->nonDivideBuy[] = $item;
        $this->removeIds[] = $item->getId();
      }
    }

    return $this->itemArray($itemCount);
  }

  public function loadQuote($quoteId)
  {
    return $this->quoteLoader->create()->load($quoteId);
  }

  /**
   * Used to load product by id.
   *
   * @param  mixed  $id
   *
   * @return mixed
   */
  public function loadProduct($id)
  {
    return $this->productHelper->getProductLoader()->create()->load($id);
  }

  /**
   * @return array
   */
  public function getDivideBuyItems(): array
  {
    return $this->divideBuyItems;
  }

  /**
   * @return array
   */
  public function getOtherItems(): array
  {
    return $this->otherItems;
  }

  public function getCheckoutSession()
  {
    return $this->sessionHelper->getCheckoutSession();
  }

  public function getDivideBuyTotal()
  {
    $grandTotal = 0;

    $this->getItemArray();
    $otherItems = $this->getOtherItems();

    if (!empty($otherItems)) {
      $items = $this->getDivideBuyItems();
      if (!empty($items)) {
        foreach ($items as $item) {
          $productTotalPrice = $item->getRowTotalInclTax();
          $productDiscount = $item->getDiscountAmount();
          $grandTotal = $grandTotal + $productTotalPrice - $productDiscount;
        }
      }
    } else {
      $grandTotal = $this->getCheckoutSession()->getQuote()->getGrandTotal();
    }

    return (float) $grandTotal;
  }

  /**
   * Returns true if checkout with coupon code is enabled.
   *
   * @return bool $flag
   */
  public function checkoutWithCouponCode()
  {
    $couponCode = $this->sessionHelper->getQuote()->getCouponCode();
    $checkoutWithCouponCodeFlag = $this->sessionHelper->getStoreConfigHelper()->getCheckoutWithCouponCodeFlag();

    $flag = 1;
    if (!$checkoutWithCouponCodeFlag && !empty($couponCode)) {
      $flag = 0;
    }

    return $flag;
  }

  /**
   * Used to check that current divide buy product total is greater than the min order amount or not.
   *
   * @return bool
   */
  public function checkMinOrderAmount(): bool
  {
    if ($this->sessionHelper->getStoreConfigHelper()->getMinOrderAmount() > $this->getDivideBuyTotal()) {
      return false;
    }

    return true;
  }

  /**
   * Used to check that current divide buy product total is greater than the max order amount or not.
   *
   * @return bool
   */
  public function checkMaxOrderAmount(): bool
  {
    if ($this->sessionHelper->getStoreConfigHelper()->getMaxOrderAmount() < $this->getDivideBuyTotal()) {
      return false;
    }

    return true;
  }

  public function getMinOrderAmount()
  {
    return $this->sessionHelper->getStoreConfigHelper()->getMinOrderAmount();
  }

  public function getMaxOrderAmount()
  {
    return $this->sessionHelper->getStoreConfigHelper()->getMaxOrderAmount();
  }

  /**
   * Used to fetch the available instalments as per grand total.
   *
   * @return string
   */
  public function getAvailableInstalments(): string
  {
    // Getting cart total from request.
    $grandTotal = $this->getDivideBuyTotal();

    $instalmentHtml = "<div class='dividebuy-payment-description'>";


    if($grandTotal < $this->getMinOrderAmount()){
      $instalmentHtml .= "<div class='dividebuy_instalment_mininfo'>";
      $instalmentHtml .= "<span>The minimum order value to pay with DivideBuy is £" . $this->getMinOrderAmount() . "</span>";
      $instalmentHtml .= '</div>';
    }

    else if($grandTotal > $this->getMaxOrderAmount()){
      $instalmentHtml .= "<div class='dividebuy_instalment_maxinfo'>";
      $instalmentHtml .= "<span>The maximum order value to pay with DivideBuy is £" . $this->getMaxOrderAmount() . "</span>";
      $instalmentHtml .= '</div>';
    }

    else{

      // Getting retailer instalments.
      $instalments = $this->sessionHelper->getStoreConfigHelper()->getInstallments();
      $instalments_ibc = $this->sessionHelper->getStoreConfigHelper()->getIbcInstallments();
      $instalments = array_merge($instalments,$instalments_ibc);
      usort($instalments, function ($a, $b) {
        return $a['key'] <=> $b['key'];
      });

      
      $instalmentHtml .= "<p class='payment-title'>Choose your monthly payment plan:</p>";
      $instalmentHtml .= "<p class='payment-note'>Note: your first instalment will be collected on completion of your order.</p>";

      $instalmentHtml .= "<div class='dividebuy-checkout-row dividebuy-checkout-header'>";
      $instalmentHtml .= "<div class='dividebuy_instalment_data'>";
      $instalmentHtml .= "<div class='data'>";
      $instalmentHtml .= "<span class='dividebuy-blankspace interest-input'></span>";
      $instalmentHtml .= "<span class='interest-main-figure'>Months</span>";
      $instalmentHtml .= '</div>';
      $instalmentHtml .= '</div>';

      $instalmentHtml .= "<div class='dividebuy_instalment_data-2'>";
      $instalmentHtml .= "<div class='data'>";
      $instalmentHtml .= "<span class='interest-digit'>In Total<br />You’ll Pay</span>";
      $instalmentHtml .= '</div>';
      $instalmentHtml .= "<div class='data'>";
      $instalmentHtml .= "<span class='interest-digit'>Interest Rate (Fixed) / APR</span>";
      $instalmentHtml .= '</div>';
      $instalmentHtml .= "<div class='data'>";
      $instalmentHtml .= "<span class='interest-digit'>Monthly Payment</span>";
      $instalmentHtml .= '</div>';
      $instalmentHtml .= '</div>';
      $instalmentHtml .= '</div>';

      $i = 0;
      $getBlock = $this->_layout->createBlock("Dividebuy\CheckoutConfig\Block\Cart");

      foreach ((array) $instalments as $instalment) {
        if ($grandTotal >= $instalment['min'] && $grandTotal <= $instalment['max']) {
          if ($i === 0) {
            $class = 'dividebuy-checkout-row dividebuy-active-row instalment';
          } else {
            $class = 'dividebuy-checkout-row instalment';
          }
          $instalmentHtml .= "<div class='".$class."'>";
          $installments_details =$getBlock->calculateInstalmentAmountsForLoan($grandTotal,$instalment['interest_rate']/100,$instalment['key']);
          $instalmentHtml .= "<div class='dividebuy_instalment_data'>";
          $instalmentHtml .= "<div class='data'>";
          $instalmentHtml .= "<span class='interest-input'>";
          if ($i === 0) {
            $instalmentHtml .= "<input class='dividebuy-input-radio instalment' type='radio' name='dividebuy_available_instalments' value='".$instalment['key']."' checked='checked' />";
          } else {
            $instalmentHtml .= "<input class='dividebuy-input-radio instalment' type='radio' name='dividebuy_available_instalments' value='".$instalment['key']."'/>";
          }
          $instalmentHtml .= '</span>';
          $instalmentHtml .= "<div class='interest-main-figure'>";
          $instalmentHtml .= "<span class='interest-figure'>".$instalment['key'].'</span>';
          $instalmentHtml .= '</div>';
          $instalmentHtml .= '</div>';
          $instalmentHtml .= '</div>';

          $instalmentHtml .= "<div class='dividebuy_instalment_data-2'>";

          if($instalment['interest_rate'] > 0){
            $instalmentHtml .= "<div class='data'>";
            $instalmentHtml .= "<span class='interest-digit'>£".number_format($installments_details['totalToPay'], 2)."</span>";
            $instalmentHtml .= '</div>';
          }
          else{
            $instalmentHtml .= "<div class='data'>";
            $instalmentHtml .= "<span class='interest-digit'>£".number_format($grandTotal, 2)."</span>";
            $instalmentHtml .= '</div>';
          }

          $instalmentHtml .= "<div class='data'>";
          $interestRate =  ($instalment['interest_rate'] /100) * $grandTotal;
          $instalmentHtml .= "<span class='interest-digit'>".number_format((float)$instalment['interest_rate'], 2)."%</span>";
          $instalmentHtml .= '</div>';

          $instalmentHtml .= "<div class='data'>";
          $instalmentHtml .= "<span class='interest-figure'> £".number_format($installments_details['regularMonthlyInstalment'] , 2)."</span>";
          $instalmentHtml .= '</div>';
          $instalmentHtml .= '</div>';
          $instalmentHtml .= '</div>';
          ++$i;
        }
      }
      $instalmentHtml .= "<p class='security-reason'>For security reasons, your billing and delivery addresses must match if choosing interest free credit with DivideBuy.</p>";

    }
    $instalmentHtml .= '</div>';
    return $instalmentHtml;
  }

  public function getSymbol(): string
  {
    return $this->productHelper->getSymbol();
  }

  public function getLogger(): Logger
  {
    return $this->sessionHelper->getStoreConfigHelper()->getLogger();
  }

  /**
   * @param mixed $cartId
   *
   * @return CartInterface | Quote
   *
   * @throws NoSuchEntityException
   */
  public function getCartById($cartId)
  {
    return $this->cartRepo->get($cartId);
  }

  public function createEmptyCart()
  {
    return $this->cartManager->createEmptyCart();
  }

  /**
   * @param mixed $quoteId
   *
   * @return mixed
   *
   * @throws CouldNotSaveException
   */
  public function placeCartOrder($quoteId)
  {
    return $this->cartManager->placeOrder($quoteId);
  }

  /**
   * Used to get array of cart items with the count of divide buy and non-divide buy product.
   *
   * @param  int  $itemCount
   *
   * @return array
   */
  private function itemArray($itemCount = null): array
  {
    $jsonData = $this->sessionHelper->getStoreConfigHelper()->getJsonHelper()->encode($this->removeIds);
    $this->cartSplit['cart_item'] = $itemCount;

    $this->divideBuyItems = $this->divideBuy;
    $this->otherItems = $this->nonDivideBuy;

    $this->cartSplit['dividebuy'] = count($this->divideBuy);
    $this->cartSplit['nodividebuy'] = count($this->nonDivideBuy);
    $this->cartSplit['remove_ids'] = $jsonData;
    $this->cartSplit['dividebuyIds'] = $this->divideBuyIds;

    return $this->cartSplit;
  }
}
