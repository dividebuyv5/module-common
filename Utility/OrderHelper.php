<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Magento\Framework\DataObject;
use Dividebuy\Common\Exception\InvalidParameterException;
use Dividebuy\Common\Logger\Logger;
use Exception;
use Laminas\Stdlib\Parameters;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\OrderNotifier;

class OrderHelper
{
  private OrderNotifier $orderNotifier;
  private ApiHelper $apiHelper;
  private Order $orderModel;
  private Logger $logger;
  private OrderModel $orderRepository;

  public function __construct(
      Order $orderModel,
      ApiHelper $apiHelper,
      OrderNotifier $orderNotifier,
      Logger $logger,
      OrderModel $orderRepository
  ) {
    $this->orderModel = $orderModel;
    $this->apiHelper = $apiHelper;
    $this->orderNotifier = $orderNotifier;
    $this->logger = $logger;
    $this->orderRepository = $orderRepository;
  }

  public function processCancelOrderRequest(Order $order, $isSuccess = true): array
  {
    try {
      $order->setState(Order::STATE_PROCESSING);
      $order->setStatus('pending');
      $order->setBaseDiscountCanceled(0);
      $order->setBaseShippingCanceled(0);
      $order->setBaseSubtotalCanceled(0);
      $order->setBaseTaxCanceled(0);
      $order->setBaseTotalCanceled(0);
      $order->setDiscountCanceled(0);
      $order->setShippingCanceled(0);
      $order->setSubtotalCanceled(0);
      $order->setTaxCanceled(0);
      $order->setTotalCanceled(0);

      $allItems = $order->getAllItems() ?: [];

      foreach ($allItems as $item) {
        $item->setQtyCanceled(0);
        $item->setTaxCanceled(0);
        $item->setHiddenTaxCanceled(0);
        $item->save();
      }

      $order->save();

      if ($isSuccess) {
        return $this->processSuccessOrder($order);
      }

      return [
          'error' => 0,
          'success' => 1,
          'message' => 'Cancelled order is successfully accepted.',
          'status' => 'ok',
      ];
    } catch (Exception $e) {
      $this->logger->error((string) $e);
      throw new InvalidParameterException('There is a problem in accepting this cancel order.', 402);
    }
  }

  public function processSuccessOrder($completeOrder): array
  {
    $orderDetails = $this->apiHelper->getSdkApi()->getUserOrder((string) $completeOrder->getId());

    if (empty($orderDetails)) {
      return [
          'error' => 0,
          'success' => 1,
          'message' => 'Order not found in checkout',
          'status' => 'ok',
      ];
    }

    // Calling complete order for setting latest order data
    $this->completeOrder($completeOrder, $orderDetails);
    $this->apiHelper->getSdkApi()->syncRetailerOrder((int) $completeOrder->getId());

    $completeOrder->setState(Order::STATE_PROCESSING);
    $completeOrder->setStatus(Order::STATE_PROCESSING);
    $completeOrder->save();

    return [
        'error' => 0,
        'success' => 1,
        'message' => 'Cancelled order is successfully accepted.',
        'status' => 'ok',
    ];
  }

  public function completeOrder($order, $orderDetails): OrderModel
  {
    $orderParams = new DataObject($orderDetails);
    $orderTime = $orderParams->getDataByKey('orderTime');
    $orderReferenceId = $orderParams->getDataByKey('laravel_order_ref_id');
    $address = $orderParams->getDataByKey('address', []);

    $addressParam = new DataObject($address);

    $street1 = $addressParam->getDataByKey('house_number').' '.$addressParam->getDataByKey('house_name').', '.$addressParam->getDataByKey('street');
    $street1 = ltrim($street1, ', ');
    $street2 = $addressParam->getDataByKey('address2');
    $street = [
        '0' => $street1,
        '1' => $street2,
    ];

    $isPhoneOrderEnabled = $orderParams->getDataByKey('is_phone_order_enabled');
    $customerEmail = $orderParams->getDataByKey('customer_email');
    $userAddress = [
        'prefix' => $addressParam->getDataByKey('prefix'),
        'firstname' => $addressParam->getDataByKey('first_name'),
        'lastname' => $addressParam->getDataByKey('last_name'),
        'street' => $street,
        'postcode' => $addressParam->getDataByKey('postcode'),
        'region' => $addressParam->getDataByKey('region'),
        'city' => $addressParam->getDataByKey('city'),
        'email' => $customerEmail,
        'telephone' => $addressParam->getDataByKey('contact_number'),
    ];
    $order->setCreatedAt($orderTime);
    // Checking if order is placed via phone order.
    if ($isPhoneOrderEnabled === 1) {
      $order->addStatusHistoryComment(
          'DivideBuy order authenticated via phone order. Transaction ID : "'.$orderReferenceId.'"',
          OrderModel::STATE_PROCESSING
      );
    } else {
      $order->addStatusHistoryComment(
          'DivideBuy order authenticated. Transaction ID : "'.$orderReferenceId.'"',
          OrderModel::STATE_PROCESSING
      );
    }
    $order->save();

    // Setting Order with address

    $this->setOrderData($order, $userAddress);
    $this->orderNotifier->notify($order);

    // Sending retailer email
    return $order;
  }

  /**
   * Used set order details.
   *
   * @param       $order
   * @param  array  $userAddress
   *
   * @return array|false
   */
  public function setOrderData($order, $userAddress)
  {
    // Saving Customer Information
    $addressParam = new Parameters($userAddress);
    $order->setCustomerFirstname($addressParam->get('firstname'));
    $order->setCustomerLastname($addressParam->get('lastname'));
    $order->setCustomerEmail($addressParam->get('email'));

    try {
      $order->setState(OrderModel::STATE_PROCESSING, true);
      $order->setStatus(OrderModel::STATE_PROCESSING);
      $order->save();
    } catch (Exception $ex) {
      return false;
    }

    $orderShipping = $this->populateAddressDetails($order->getShippingAddress(), $addressParam);
    $orderBilling = $this->populateAddressDetails($order->getBillingAddress(), $addressParam);

    try {
      $order->save();
      $orderShipping->save();
      $orderBilling->save();

      return [
          'error' => 0,
          'success' => 1,
          'status' => 'ok',
          'order_id' => $order->getId(),
          'message' => 'Order placed successfully',
      ];
    } catch (Exception $e) {
      return [
          'error' => 1,
          'order_id' => $order->getId(),
          'message' => 'order not found',
      ];
    }
  }

  public function loadOrderById($orderId): ?OrderModel
  {
    return $this->orderRepository->load($orderId);
  }

  /**
   * Used to validate the order is divide buy or not.
   *
   * @param  OrderModel  $order
   *
   * @return bool
   */
  public function validateDivideBuyOrder(OrderModel $order): bool
  {
    $payment = $order->getPayment();
    if (!empty($payment)) {
      $methodCode = $payment->getMethod();

      if ($methodCode === DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
        return true;
      }
    }

    return false;
  }

  public function getOrderNotifier(): OrderNotifier
  {
    return $this->orderNotifier;
  }

  private function populateAddressDetails($address, Parameters $addressParam)
  {
    $address->setPrefix($addressParam->get('prefix'));
    $address->setFirstname($addressParam->get('firstname'));
    $address->setLastname($addressParam->get('lastname'));
    $address->setStreet($addressParam->get('street'));
    $address->setPostcode($addressParam->get('postcode'));
    $address->setRegion($addressParam->get('region'));
    $address->setCity($addressParam->get('city'));
    $address->setEmail($addressParam->get('email'));
    $address->setTelephone($addressParam->get('telephone'));

    return $address;
  }

  public function validateOrderStore($order_id,$store_code) {
    $order_store_code = $this->orderModel->getStore()->getStoreId();
    if($order_store_code !== $store_code){
        throw new InvalidParameterException('Order not found for entered store code', 404);
    }
    return true;
  }
}
