<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Dividebuy\Common\Registry;
use Magento\Framework\App\Action\Context;
use Throwable;

class ResponseHelper
{
  private StoreConfigHelper $configHelper;
  private Context $actionContext;
  private Registry $registry;

  public function __construct(
      Context $actionContext,
      StoreConfigHelper $configHelper,
      Registry $registry
  ) {
    $this->actionContext = $actionContext;
    $this->configHelper = $configHelper;
    $this->registry = $registry;
  }

  public function sendJsonResponse($data)
  {
    $response = $this->actionContext->getResponse();
    $response->setHeader('Content-type', 'application/json', true);

    return $response->setBody(json_encode($data));
  }

  public function sendTextResponse($body, $type = 'html')
  {
    $response = $this->actionContext->getResponse();
    $response->setHeader('Content-type', 'text/'.$type);

    return $response->setBody($body);
  }

  public function debugResponse(
      $response,
      $title = '',
      ?Throwable $exception = null,
      array $params = []
  ): ResponseHelper {
    if ($this->configHelper->isLoggable()) {
      $dataResponse = "============\n";
      $dataResponse .= "Message : {$title} \n";
      if ($exception) {
        $dataResponse .= 'Error message :'.$exception->getMessage()."\n";
      }
      if ($params) {
        $dataResponse .= 'Params :'.$this->jsonEncode($params)."\n";
      }
      $dataResponse .= 'Response :'.$this->jsonEncode($response)."\n";
      $this->configHelper->getLogger()->info($dataResponse);
    } elseif ($exception) {
      $this->configHelper->getLogger()->debug((string) $exception);
    }

    return $this;
  }

  public function jsonEncode($data)
  {
    return $this->configHelper->getJsonHelper()->encode($data);
  }

  public function jsonDecode($data)
  {
    return $this->configHelper->getJsonHelper()->decode($data);
  }

  public function getConfigHelper(): StoreConfigHelper
  {
    return $this->configHelper;
  }

  public function getRegistry(): Registry
  {
    return $this->registry;
  }

  public function getRequestStreamData(): string
  {
    return trim((string) file_get_contents('php://input')) ?: '[]';
  }
}
