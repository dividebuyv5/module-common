<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Constants\XmlFilePaths;
use Magento\Framework\DataObject;
use Dividebuy\Common\Logger\Logger;
use Dividebuy\Common\Utility\JsonHelper as JsonData;
use Dividebuy\Common\StoreInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\App\Request\Http;
use Throwable;

class StoreConfigHelper
{
  public const DEFAULT_STORE_ID = null;

  private Logger $logger;
  private ScopeConfigInterface $scopeConfig;
  private StoreManagerInterface $storeManager;
  private JsonData $jsonHelper;
  private ResourceConfig $resourceConfig;
  /**
     * @var \Magento\Framework\Serialize\Serializer\Serialize
     */
    private $serialize;

  /**
   * @var mixed
   */
  private $storeId;
  private Http $request;

  public function __construct(
      ScopeConfigInterface $scopeConfig,
      Logger $logger,
      StoreManagerInterface $storeManager,
      JsonData $jsonHelper,
      ResourceConfig $resourceConfig,
      \Magento\Framework\Serialize\Serializer\Serialize $serialize,
      Http $request
  ) {
    $this->scopeConfig = $scopeConfig;
    $this->logger = $logger;
    $this->storeManager = $storeManager;
    $this->jsonHelper = $jsonHelper;
    $this->resourceConfig = $resourceConfig;
    $this->serialize = $serialize;
    $this->request = $request;
  }

  public function isLoggable($storeId = self::DEFAULT_STORE_ID): bool
  {
    return (bool) $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_LOG_ERRORS,
        ScopeInterface::SCOPE_STORE,
        empty($storeId) ? self::DEFAULT_STORE_ID : $storeId
    );
  }

  /**
   * Get list of postcodes in which DivideBuy shipment is not available.
   * Checks whether the given postcode is deliverable or not.
   *
   * @param  string  $zipcode
   *
   * @return bool
   */
  public function isPostCodeDeliverable(string $zipcode): bool
  {
    $postCodes = $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_DIVIDEBUY_EXCLUDEPOSTCODE,
        ScopeInterface::SCOPE_STORE
    );

    if (!empty($postCodes)) {
      $nonDeliverableZipCodes = explode(',', $postCodes);
      $zipcode = strtolower(trim($zipcode));
      $zipcode = str_replace(' ', '', $zipcode);
      $size = count($nonDeliverableZipCodes);

      for ($i = 0; $i < $size; ++$i) {
        $nonDeliverableZipCodes[$i] = strtolower(trim($nonDeliverableZipCodes[$i]));
        $nonDeliverableZipCodes[$i] = str_replace(' ', '', $nonDeliverableZipCodes[$i]);
        $postCodeMatchLength = strlen($nonDeliverableZipCodes[$i]);
        $zipCodeMatch = substr($zipcode, 0, $postCodeMatchLength);
        if ($zipCodeMatch === $nonDeliverableZipCodes[$i]) {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Checking authentication.
   *
   * @param  mixed  $token
   * @param  mixed  $authentication
   * @param  int  $storeId
   *
   * @return array | bool
   */
  public function isFailedAuth($token, $authentication, $storeId = self::DEFAULT_STORE_ID)
  {
    $tokenNumber = $this->getStoreToken($storeId);
    $authenticationKey = $this->getAuthenticationKey($storeId);

    if (!$token || !$authentication || $token !== $tokenNumber || $authentication !== $authenticationKey) {
      return [
          'error' => 1,
          'success' => 0,
          'message' => 'Store Authentication Failed',
          'status' => '401',
      ];
    }

    return false;
  }

  public function getStoreToken($storeId = self::DEFAULT_STORE_ID): string
  {
    return (string) $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_TOKEN_NUMBER,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getAuthenticationKey($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(DivideBuy::DIVIDEBUY_AUTH_NUMBER, ScopeInterface::SCOPE_STORE, $storeId);
  }

  public function getOrderUrlDomainPrefix($storeId): string
  {
    return $this->scopeConfig->getValue(DivideBuy::DIVIDEBUY_DOMAIN_URL_PREFIX, ScopeInterface::SCOPE_STORE, $storeId);
  }

  public function getEnvironment($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(DivideBuy::DIVIDEBUY_ENVIRONMENT, ScopeInterface::SCOPE_STORE, $storeId);
  }

  public function generateSplashKey($orderId): string
  {
    $tokenNumber = $this->getStoreToken();
    $authenticationKey = $this->getAuthenticationKey();
    $retailerId = $this->getRetailerId();

    $splashKeyLeft = base64_encode($tokenNumber.':'.$orderId);
    $splashKeyRight = base64_encode($authenticationKey.':'.$retailerId);

    return base64_encode($splashKeyLeft.':'.$splashKeyRight);
  }

  public function decodeSplashKey($splashKey)
  {
    $splashKeyDecode = base64_decode($splashKey);
    $splashKeyExplode = explode(':', $splashKeyDecode);
    $splashKeyLeft = base64_decode($splashKeyExplode[0]);

    return explode(':', $splashKeyLeft);
  }

  public function getRetailerId($storeId = null)
  {
    return $this->scopeConfig->getValue(XmlFilePaths::XML_PATH_RETAILER_ID, ScopeInterface::SCOPE_STORE, $storeId);
  }

  /**
   * Function used for getting background color.
   */
  public function getBannerBackgroundColor(): string
  {
    return (string) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_BANNER_BACKGROUND_COLOR,
        ScopeInterface::SCOPE_STORE,
        $this->getStoreId()
    );
  }

  public function getStoreScopedValue($key, $storeId = null)
  {
    return $this->scopeConfig->getValue(
        $key,
        ScopeInterface::SCOPE_STORE,
        $storeId ? $storeId : $this->getStoreId()
    );
  }

  public function getCurrentStoreId(){
    try {
      $current_store_id = $this->request->getParam('store', 0);
      return $current_store_id;
    } catch (Throwable $exception) {
      $this->logError($exception);
    }
    return '';
  }

  /**
   * @return int|mixed|string
   */
  public function getStoreId()
  {
    try {
      if (!$this->storeId) {
        $this->storeId = $this->storeManager->getStore()->getId();
      }

      return $this->storeId;
    } catch (Throwable $exception) {
      $this->logError($exception);
    }

    return '';
  }

  /**
   * Returns the configuration value of Set new products to shop with DivideBuy of Divide buy configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getProductDivideBuyEnableDefaultConfigValue($storeId = null)
  {
    return $this->scopeConfig
        ->getValue(
            XmlFilePaths::XML_PATH_PRODUCT_DVIDEBUY_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
  }

  public function logError($exception): StoreConfigHelper
  {
    $this->getLogger()->error((string) $exception);

    return $this;
  }

  /**
   * Function for getting maximum month instalment.
   *
   * @return mixed
   */
  public function getMaximumAvailableInstalments()
  {
    $instalments = $this->getInstallments();
    $instalments_ibc = $this->getIbcInstallments();
    $instalments = array_merge($instalments,$instalments_ibc);
    $maxInstalmentArray = array();
    foreach ($instalments as $instalment) 
    {
      $maxInstalmentArray[] = $instalment['key'];
    };
    return max($maxInstalmentArray);
  }

  public function getInstallments()
  {
    if($this->getStoreId()){
      return $this->serialize->unserialize(
        $this->scopeConfig->getValue(
            DivideBuy::DIVIDEBUY_INSTALMENTS,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        )
      );
    }else{
      return $this->serialize->unserialize(
        $this->scopeConfig->getValue(
            DivideBuy::DIVIDEBUY_INSTALMENTS,
            ScopeInterface::SCOPE_STORE,
            self::DEFAULT_STORE_ID
        )
      );
    }
    
  }

  public function getIbcInstallments()
  {
    if( $this->scopeConfig->getValue(DivideBuy::DIVIDEBUY_IBC_INSTALMENTS,ScopeInterface::SCOPE_STORE,$this->getStoreId()) ){
      return $this->serialize->unserialize(
        $this->scopeConfig->getValue(
            DivideBuy::DIVIDEBUY_IBC_INSTALMENTS,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        )
      );
    }elseif ($this->scopeConfig->getValue(DivideBuy::DIVIDEBUY_IBC_INSTALMENTS,ScopeInterface::SCOPE_STORE,self::DEFAULT_STORE_ID)) {
      return $this->serialize->unserialize(
        $this->scopeConfig->getValue(
            DivideBuy::DIVIDEBUY_IBC_INSTALMENTS,
            ScopeInterface::SCOPE_STORE,
            self::DEFAULT_STORE_ID
        )
      );
    }
    else{
      return array();
    }
    
  }

  public function GetAprRates()
  {
    $storeId = $storeId ?? $this->getStoreId();

    return (string) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_APR_RATE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function GetAvgIbcTermLength()
  {
    $storeId = $storeId ?? $this->getStoreId();

    return (float) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_AVERAGE_IBC_TERM_LENGTH,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function GetAvgIbcOrderValue()
  {
    $storeId = $storeId ?? $this->getStoreId();

    return (float) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_AVERAGE_IBC_ORDER_VALUE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getTermsAndConditionsUrl()
  {
    $storeId = $storeId ?? $this->getStoreId();

    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_Terms_Condition_URL,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieve product banner url.
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getProductBannerUrl(): string
  {
    return (string) $this->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).
      DivideBuy::DIVIDEBUY_MEDIA_DIR.$this->getProductBanner();
  }

  /**
   * @return \Magento\Store\Api\Data\StoreInterface | StoreInterface
   *
   * @throws NoSuchEntityException
   */
  public function getStore()
  {
    return $this->storeManager->getStore();
  }

  /**
   * Retrieve product banner.
   *
   * @param  null  $storeId
   *
   * @return string
   */
  public function getProductBanner($storeId = self::DEFAULT_STORE_ID): string
  {
    return (string) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_PRODUCT_BANNER_IMAGE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieve retailer logo url.
   *
   * @throws NoSuchEntityException
   */
  public function getRetailerLogoUrl(): string
  {
    $retailerUrl = $this->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).
      'dividebuy/'.
      $this->getRetailerImage();

    $secureFlag = parse_url($retailerUrl, PHP_URL_SCHEME);

    if ($secureFlag !== 'https') {
      $retailerUrl = $this->scopeConfig->getValue(
          DivideBuy::DIVIDEBUY_LOGO_URL,
          ScopeInterface::SCOPE_STORE,
          $this->getStoreId()
      );
    }

    return (string) $retailerUrl;
  }

  public function getRetailerImage($storeId = self::DEFAULT_STORE_ID): string
  {
    return (string) $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_RETAILER_IMAGE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getBannerImage($storeId = self::DEFAULT_STORE_ID)
  {
    return (string) $this->getValue(
        DivideBuy::DIVIDEBUY_BANNER_IMAGE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Get base URL.
   *
   * @param  string  $scope
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getBaseUrl($scope = self::DEFAULT_STORE_ID): string
  {
    return (string) $this->storeManager->getStore()->getBaseUrl($scope);
  }

  /**
   * Retrieve store name.
   *
   * @param  null  $storeId
   *
   * @return string
   */
  public function getStoreName($storeId = self::DEFAULT_STORE_ID): string
  {
    $storeId = $storeId ?? $this->getStoreId();

    return (string) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_STORE_NAME,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Function used for checking if softsearch is enabled on cart page.
   *
   * @param  null  $storeId
   *
   * @return mixed
   */
  public function isCartSoftSearchEnabled($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_SOFT_SEARCH,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Function for getting maximum month instalment value.
   *
   * @return mixed
   */
  public function getMaximumAvailableInstalmentValue()
  {
    $instalments = $this->serialize->unserialize(
        $this->scopeConfig->getValue(
            DivideBuy::DIVIDEBUY_INSTALMENTS,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        )
    );
    $maxInstalmentValueArray = [];

    foreach ($instalments as $instalment) {
      $maxInstalmentValueArray[] = $instalment['value'];
    }

    return max($maxInstalmentValueArray);
  }

  /**
   * Retrieves minimum order amount from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getMinOrderAmount($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_ORDER_MIN_AMOUNT,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves maximum order amount from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getMaxOrderAmount($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_ORDER_MAX_AMOUNT,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieve product status.
   *
   * @param  null  $storeId
   *
   * @return bool
   */
  public function getProductStatus($storeId = null): bool
  {
    $isIPAllowed = $this->isIpAllowed($storeId);
    $paymentActive = $this->isDivideBuyPaymentActive($storeId);
    $productStatus = $this->getStoreScopedValue(XmlFilePaths::XML_PATH_PRODUCT_ENABLED, $storeId);
    $extensionStatus = $this->getExtensionStatus($storeId);

    if ($productStatus && $paymentActive && $isIPAllowed && $extensionStatus) {
      return true;
    }

    return false;
  }

  public function isDivideBuyPaymentActive($storeId = null)
  {
    return $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_DBPAYMENT_ACTIVE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves cart status from configuration.
   *
   * @param  null  $storeId
   *
   * @return bool
   */
  public function getCartStatus($storeId = self::DEFAULT_STORE_ID): bool
  {
    $isIpAllowed = $this->isIpAllowed($storeId);

    $isDivideBuyPaymentActive = $this->isDivideBuyPaymentActive($storeId);
    $cartStatus = $this->getStoreScopedValue(XmlFilePaths::XML_PATH_CART_ENABLED, $storeId);
    $extensionStatus = $this->getExtensionStatus($storeId);

    return $cartStatus && $isDivideBuyPaymentActive && $isIpAllowed && $extensionStatus;
  }

  public function getExtensionStatus($storeId)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_DIVIDEBUY_EXTENSION_STATUS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Used to check whether current ip is allowed to see Dividebuy functionality or not.
   *
   * @param  mixed  $storeId
   *
   * @return bool
   */
  public function isIpAllowed($storeId = self::DEFAULT_STORE_ID): bool
  {
    $serverData = new DataObject($_SERVER ?? []);
    $serverIP = $serverData->getDataByKey('REMOTE_ADDR');
    $allowedIP = (string) $this->getAllowedIps($storeId);
    $allowedIpArray = array_map('trim', explode(',', $allowedIP));

    if (!empty($allowedIpArray) && $allowedIP !== '') {
      if (in_array($serverIP, $allowedIpArray)) {
        $isIPAllowed = true;
      } else {
        $isIPAllowed = false;
      }
    } else {
      $isIPAllowed = true;
    }

    return $isIPAllowed;
  }

  public function getAllowedIps($storeId): string
  {
    return (string) $this->scopeConfig
        ->getValue(
            DivideBuy::DIVIDEBUY_ALLOWED_IP_LIST,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
  }

  /**
   * Returns the flag for checkout with coupon code.
   *
   * @return mixed
   */
  public function getCheckoutWithCouponCodeFlag()
  {
    return $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_ALLOW_CHECKOUT_WITH_COUPON,
        ScopeInterface::SCOPE_STORE,
        $this->getStoreId()
    );
  }

  /**
   * Retrieves button image url from configuration.
   *
   * @throws NoSuchEntityException
   */
  public function getButtonImageUrl(): string
  {
    return $this->storeManager->getStore()->getBaseUrl(
        UrlInterface::URL_TYPE_MEDIA
    ).DivideBuy::DIVIDEBUY_MEDIA_DIR.$this->getButtonImage();
  }

  public function getButtonImage($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_BUTTON_IMAGE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves cart position from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getCartPosition($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_POSITION,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves product page text from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getProductPromotionText($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_PRODUCT_PROMOTIONS_TEXT,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getFinanceCalculation($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_PRODUCT_FINANCE_CALCULATION,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves button hover image url from configuration.
   *
   * @return mixed
   *
   * @throws NoSuchEntityException
   */
  public function getButtonHoverImageUrl(): string
  {
    return $this->storeManager->getStore()->getBaseUrl(
        UrlInterface::URL_TYPE_MEDIA
    ).DivideBuy::DIVIDEBUY_MEDIA_DIR.$this->getButtonHoverImage();
  }

  /**
   * Retrieves button hover image from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getButtonHoverImage($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_BUTTON_HOVER,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieves the button css value from configuration.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getButtonCss($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_BUTTON_CSS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getPrefixLabel($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_PREFIX_LABEL,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getPrefixCss($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_PREFIX_CSS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getSuffixLabel($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_SUFFIX_LABEL,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getSuffixCss($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CART_SUFFIX_CSS,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getJsonHelper(): JsonHelper
  {
    return $this->jsonHelper;
  }

  /**
   * Returns the flag for in store collection logic.
   *
   * @return mixed
   */
  public function getAutoCheckoutStatus()
  {
    $storeId = $this->getStoreId();

    return $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_RETAILER_AUTO_CHECKOUT,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * @param  bool  $fromStore
   *
   * @return mixed
   *
   * @throws NoSuchEntityException
   */
  public function getStoreUrl($fromStore = true)
  {
    return $this->storeManager->getStore()->getCurrentUrl($fromStore);
  }

  /**
   * Function used for checking if softsearch is enabled on checkout page.
   *
   * @param  null  $storeId
   *
   * @return mixed
   */
  public function isCheckoutSoftSearchEnabled($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_CHECKOUT_SOFT_SEARCH,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  /**
   * Retrieve Checkout banner url.
   *
   * @throws NoSuchEntityException
   */
  public function getCheckoutBannerUrl(): string
  {
    return (string) $this->storeManager->getStore()->getBaseUrl(
        UrlInterface::URL_TYPE_MEDIA
    ).DivideBuy::DIVIDEBUY_MEDIA_DIR.$this->getCheckoutButtonImage();
  }

  public function getVatClass()
  {
    return $this->scopeConfig->getValue(DivideBuy::DIVIDEBUY_TAX_CLASS, ScopeInterface::SCOPE_STORE);
  }

  /**
   * Decides whether vat is applied before or after Vat.
   *
   * @param  int  $storeId
   *
   * @return string
   */
  public function getDiscountVatStatus($storeId = self::DEFAULT_STORE_ID): string
  {
    if ($this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_TAX_CALCULATION_DISCOUNT,
        ScopeInterface::SCOPE_STORE,
        $storeId
    )) {
      return 'afterVat';
    }

    return 'beforeVat';
  }

  /**
   * Used to get the retailer store Token and authentication.
   */
  public function getRetailerStoreTokenAndAuthentication(): array
  {
    return [
        'store_token' => $this->getStoreToken(),
        'store_authentication' => $this->getAuthenticationKey(),
    ];
  }

  /**
   * Used to get google analytics unique key from configuration.
   *
   * @param  mixed  $storeId
   *
   * @return string
   */
  public function getGoogleAnalyticUniqueKey($storeId = self::DEFAULT_STORE_ID): string
  {
    return (string) $this->scopeConfig->getValue(
        XmlFilePaths::XML_PATH_GOOGLE_ANALYTICS_UNIQUE_KEY,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function isGuestCheckoutEnabled($storeId = self::DEFAULT_STORE_ID)
  {
    return $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_GUEST_CHECKOUT_ENABLED,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getValue($path, $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
  {
    return $this->scopeConfig->getValue($path, $scopeType, $scopeCode);
  }

  public function getCheckoutButtonImage($storeId = self::DEFAULT_STORE_ID): string
  {
    return (string) $this->getValue(
        DivideBuy::DIVIDEBUY_CHECKOUT_BUTTON_IMAGE,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getFlagToDeleteOrder()
  {
    return $this->scopeConfig->getValue(
        DivideBuy::DIVIDEBUY_FLAG_TO_DELETE_ORDER,
        ScopeInterface::SCOPE_STORE
    );
  }

  public function getGlobalDeactivateFlag($storeId): int
  {
    return (int) $this->getValue(
        XmlFilePaths::XML_PATH_DIVIDEBUY_GLOBAL_DEACTIVATED,
        ScopeInterface::SCOPE_STORE,
        $storeId
    );
  }

  public function getLogger(): Logger
  {
    return $this->logger;
  }

  public function getScopeConfig(): ScopeConfigInterface
  {
    return $this->scopeConfig;
  }

  public function saveConfig(
      $path,
      $value,
      $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
      $scopeId = 0
  ): StoreConfigHelper {
    $this->resourceConfig->saveConfig($path, $value, $scope, $scopeId);

    return $this;
  }
}
