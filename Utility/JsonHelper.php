<?php

declare(strict_types=1);

namespace Dividebuy\Common\Utility;

use Magento\Framework\Serialize\Serializer\Json;

class JsonHelper extends Json
{
  public function jsonDecode($data)
  {
    return $this->decode($data);
  }

  public function decode($data)
  {
    return $this->unserialize($data);
  }

  public function jsonEncode($data)
  {
    return $this->encode($data);
  }

  public function encode($data)
  {
    return $this->serialize($data);
  }
}
