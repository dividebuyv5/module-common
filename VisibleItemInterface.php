<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface VisibleItemInterface
{
  public function getProductId();

  public function getName();

  public function getSku();

  public function getQtyOrdered();

  public function getPrice();

  public function getPriceInclTax();

  public function getRowTotal();

  public function getRowTotalInclTax();

  public function getDiscountAmount();

  public function getData();

  public function getTaxPercent();
  public function getProductOptions();
}
