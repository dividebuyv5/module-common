<?php

declare(strict_types=1);

namespace Dividebuy\Common\Logger;

use Dividebuy\Common\Constants\DivideBuy;
use Monolog\Logger as MonoLogger;

class Logger extends MonoLogger
{
  public function __construct($name, array $handlers = [], array $processors = [])
  {
    $name = $name ? $name : DivideBuy::DIVIDEBUY_LOGGER_NAME;

    $handlers = $handlers ? $handlers : [
        'system' => Handler::class,
        'system_debug' => DebugHandler::class,
    ];

    parent::__construct($name, $handlers, $processors);
  }
}
