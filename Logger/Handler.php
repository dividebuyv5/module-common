<?php

declare(strict_types=1);

namespace Dividebuy\Common\Logger;

use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

class Handler extends Base
{
  /**
   * Logging level.
   *
   * @var int
   */
  protected $loggerType = Logger::INFO;

  public function __construct(File $filesystem)
  {
    parent::__construct($filesystem, null, '/var/log/Dividebuy.log');
  }
}
