<?php

declare(strict_types=1);

namespace Dividebuy\Common\Logger;

use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Logger\Handler\Base;

class DebugHandler extends Base
{
  public function __construct(File $filesystem)
  {
    parent::__construct($filesystem, null, '/var/log/DividebuyDebug.log');
  }
}
