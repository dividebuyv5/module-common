<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Customer\Model\Customer as MagentoCustomer;

abstract class Customer extends MagentoCustomer implements CustomerInterface
{
}
