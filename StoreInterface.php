<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Store\Api\Data\StoreInterface as MagentoStore;

interface StoreInterface extends MagentoStore
{
  /**
   * @param  string  $param
   *
   * @return string
   */
  public function getBaseUrl($param = '');
  public function getId();
  public function getCurrentCurrencyCode();
  public function getCurrentUrl();
}
