<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface TrackingInterface
{
  public function getShipment();
  public function getOrderId();
  public function getParentId();
  public function getTrackNumber();
  public function getTitle();
  public function getCarrierCode();
}
