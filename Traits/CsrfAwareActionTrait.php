<?php

declare(strict_types=1);

namespace Dividebuy\Common\Traits;

use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;

trait CsrfAwareActionTrait
{
  public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
  {
    return null;
  }

  public function validateForCsrf(RequestInterface $request): ?bool
  {
    return true;
  }
}
