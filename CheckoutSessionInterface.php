<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Framework\Session\SessionManagerInterface;

interface CheckoutSessionInterface extends SessionManagerInterface
{
  public function getTemparoryCart();
  public function unsTemparoryCart();
  public function getDividebuyCheckoutSession();
  public function unsDividebuyCustomQuoteId();
  public function setDividebuyCustomQuoteId($id);
  public function getShipping();
  public function setDividebuyPhoneOrderToken($installment);
  public function setSelectedInstalment($installment);
  public function setDividebuyUserEmail($email);
  public function setDividebuyCheckoutSession($value);
  public function setDividebuyOrderId($value);
  public function getLastOrderId();
  public function getDividebuyOrderId();
  public function getDividebuyPhoneOrderToken();
  public function unsDividebuyPhoneOrderToken();
  public function unsDividebuyCheckoutSession();
  public function getDividebuyUserEmail();
  public function getSelectedInstalment();
}
