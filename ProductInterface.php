<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Catalog\Api\Data\ProductInterface as MagentoProductInterface;

interface ProductInterface extends MagentoProductInterface
{
  /**
   * @return mixed
   */
  public function getProductId();

  /**
   * @return mixed
   */
  public function getParentItemId();

  /**
   * @return bool
   */
  public function getDividebuyEnable();
  public function getItemId();
  public function getProduct();
  public function getRowTotalInclTax();
  public function getDiscountAmount();
  public function getQty();
  public function save();
  public function getPriceInclTax();
  public function getShortDescription();
  public function getDividebuyTaxClass();
  public function getThumbnail();
  public function getDescription();
  public function getData($key);
  public function setWebsiteId($data);
  public function setTaxClassId($data);
}
