<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Framework\Event as MagentoEvent;

abstract class Event extends MagentoEvent implements EventInterface
{
}
