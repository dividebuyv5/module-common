<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Backend\Model\Session;

abstract class BackendSession extends Session
{
  abstract public function getPreviousRetailerImage();

  abstract public function getPreviousProductBannerImage();

  abstract public function getPreviousCheckoutButtonHoverImage();

  abstract public function getPreviousCheckoutButtonImage();

  abstract public function getPreviousExtensionStatus();

  abstract public function unsPreviousRetailerImage();

  abstract public function unsPreviousProductBannerImage();

  abstract public function unsPreviousCheckoutButtonHoverImage();

  abstract public function unsPreviousCheckoutButtonImage();

  abstract public function unsPreviousExtensionStatus();

  abstract public function setPreviousRetailerImage($data);

  abstract public function setPreviousProductBannerImage($data);

  abstract public function setPreviousCheckoutButtonHoverImage($data);

  abstract public function setPreviousCheckoutButtonImage($data);

  abstract public function setPreviousExtensionStatus($data);

  abstract public function setPreviousPaymentButtonImage($data);

  abstract public function getPreviousPaymentButtonImage();

  abstract public function unsPreviousPaymentButtonImage();
}
