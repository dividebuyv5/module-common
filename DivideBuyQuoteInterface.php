<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface DivideBuyQuoteInterface
{
  public function setPaymentMethod($method);
  public function setAppliedRuleId($ruleId);
}
