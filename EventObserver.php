<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Framework\Event\Observer;

abstract class EventObserver extends Observer implements ObserverInterface
{
}
