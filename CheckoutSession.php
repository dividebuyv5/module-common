<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Checkout\Model\Session;

abstract class CheckoutSession extends Session implements CheckoutSessionInterface
{
  abstract public function getTemparoryCart();

  abstract public function setTemparoryCart($cart);

  abstract public function getguest();

  abstract public function unsShipping();

  abstract public function setShipping($shipping);

  abstract public function getCheckoutPage();

  abstract public function setguest($guest);

  abstract public function getUseNotice($flag);
}
