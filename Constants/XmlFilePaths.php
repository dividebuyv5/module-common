<?php

declare(strict_types=1);

namespace Dividebuy\Common\Constants;

class XmlFilePaths
{
  public const XML_PATH_CART_ENABLED = 'dividebuy/cart/enabled';
  public const XML_PATH_CART_POSITION = 'dividebuy/cart/position';
  public const XML_PATH_CART_BUTTON_IMAGE = 'dividebuy/cart/button_image';
  public const XML_PATH_CART_BUTTON_HOVER = 'dividebuy/cart/button_image_hover';
  public const XML_PATH_CART_BUTTON_CSS = 'dividebuy/cart/customcss';
  public const XML_PATH_CART_PREFIX_LABEL = 'dividebuy/cart/btnprefixlbl';
  public const XML_PATH_CART_PREFIX_CSS = 'dividebuy/cart/btnprefixcss';
  public const XML_PATH_CART_SUFFIX_LABEL = 'dividebuy/cart/btnsuffixlbl';
  public const XML_PATH_CART_SUFFIX_CSS = 'dividebuy/cart/btnsuffixcss';
  public const XML_PATH_DIVIDEBUY_EXCLUDEPOSTCODE = 'dividebuy/global/exclude_post_codes';
  public const XML_PATH_ORDER_MIN_AMOUNT = 'dividebuy/global/min_order';
  public const XML_PATH_ORDER_MAX_AMOUNT = 'dividebuy/global/max_order';
  public const XML_PATH_DIVIDEBUY_COURIERS = 'dividebuy/global/couriers';
  public const XML_PATH_DIVIDEBUY_EXTENSION_STATUS = 'dividebuy/general/extension_status';
  public const XML_PATH_PRODUCT_SOFT_SEARCH = 'dividebuy/softsearch/productsoftsearch';
  public const XML_PATH_CART_SOFT_SEARCH = 'dividebuy/softsearch/cartsoftsearch';
  public const XML_PATH_CHECKOUT_SOFT_SEARCH = 'dividebuy/softsearch/checkoutsoftsearch';
  public const XML_PATH_GOOGLE_ANALYTICS_UNIQUE_KEY = 'dividebuy/general/google_analytics_unique_id';
  public const XML_PATH_DIVIDEBUY_GLOBAL_DEACTIVATED = 'dividebuy/global/deactivated';
  public const XML_PATH_DIVIDEBUY_ALLOWED_IP = 'dividebuy/general/allowed_ip';

  public const XML_PATH_PRODUCT_CUSTOM_CSS = 'dividebuy/product/customcss';
  public const XML_PATH_TOKEN_NUMBER = 'dividebuy/general/token_number';
  public const XML_PATH_AUTH_NUMBER = 'dividebuy/general/auth_number';
  public const XML_PATH_STORE_NAME = 'dividebuy/general/store_name';
  public const XML_PATH_RETAILER_LOGO = 'dividebuy/general/retailer_image';
  public const XML_PATH_BANNER_BACKGROUND_COLOR = 'dividebuy/general/button_background_color';
  public const XML_PATH_PRODUCT_DVIDEBUY_ENABLE = 'dividebuy/general/product_dividebuy_default';

  public const XML_PATH_PRODUCT_ENABLED = 'dividebuy/product/enabled';
  public const XML_PATH_PRODUCT_PROMOTIONS_TEXT  = 'dividebuy/product/promotionstext';
  public const XML_PATH_PRODUCT_FINANCE_CALCULATION  = 'dividebuy/product/finance_calculation';
  public const XML_PATH_PRODUCT_BANNER_IMAGE = 'dividebuy/product/banner_image';
  public const XML_PATH_RETAILER_ID = 'dividebuy/general/retailer_id';

  public const XML_PATH_APR_RATE = 'dividebuy/global/dividebuy_representative_APR';
  public const XML_PATH_AVERAGE_IBC_TERM_LENGTH  = 'dividebuy/global/dividebuy_average_ibc_term_length';
  public const XML_PATH_AVERAGE_IBC_ORDER_VALUE = 'dividebuy/global/dividebuy_average_ibc_order_value';

  public const XML_PATH_Terms_Condition_URL = 'dividebuy/global/terms_condition_url';
}
