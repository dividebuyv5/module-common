<?php

declare(strict_types=1);

namespace Dividebuy\Common\Constants;

class DivideBuy
{
  public const DIVIDEBUY_PAYMENT_CODE = 'dbpayment';
  public const DIVIDEBUY_LOGGER_NAME = 'DividebuyLogger';
  public const DIVIDEBUY_FLAG_TO_DELETE_ORDER = 'dividebuy/global/flag_to_delete_orders';
  public const DIVIDEBUY_RETAILER_AUTO_CHECKOUT = 'dividebuy/global/retailor_auto_checkout';
  public const DIVIDEBUY_CHECKOUT_BUTTON_IMAGE = 'payment/dbpayment/button_image';
  public const DIVIDEBUY_ORDER_STATUS = 'pending_payment';
  public const DIVIDEBUY_ALLOWED_IP_LIST = 'dividebuy/general/allowed_ip';
  public const DIVIDEBUY_LOG_ERRORS = 'dividebuy/general/allow_error_log';
  public const DIVIDEBUY_TAX_CALCULATION_DISCOUNT = 'tax/calculation/discount_tax';
  public const DIVIDEBUY_XML_PATH_POS_SHIPPING = 'carriers/dividebuyposshipping/active';
  public const DIVIDEBUY_INSTALMENTS = 'dividebuy/global/instalment_details';
  public const DIVIDEBUY_IBC_INSTALMENTS = 'dividebuy/global/instalment_ibc_details';
  public const DIVIDEBUY_COURIERS = 'dividebuy/global/couriers';
  public const DIVIDEBUY_MIN_ORDER = 'dividebuy/global/min_order';
  public const DIVIDEBUY_MEDIA_DIR = 'dividebuy/';
  public const SHIPPING_CODE = 'dividebuyposshipping';
  public const DIVIDEBUY_TOKEN_NUMBER = 'dividebuy/general/token_number';
  public const DIVIDEBUY_BANNER_IMAGE = 'dividebuy/product/banner_image';
  public const DIVIDEBUY_AUTH_NUMBER = 'dividebuy/general/auth_number';
  public const DIVIDEBUY_DOMAIN_URL_PREFIX = 'dividebuy/general/domain_url_prefix';
  public const DIVIDEBUY_ENVIRONMENT = 'dividebuy/general/environment';
  public const DIVIDEBUY_RETAILER_IMAGE = 'dividebuy/general/retailer_image';
  public const DIVIDEBUY_LOGO_URL = 'dividebuy/global/logoUrl';
  public const DIVIDEBUY_DBPAYMENT_ACTIVE = 'payment/dbpayment/active';
  public const DIVIDEBUY_ALLOW_CHECKOUT_WITH_COUPON = 'dividebuy/general/allow_checkout_with_coupon';
  public const DIVIDEBUY_TAX_CLASS = 'dividebuy/global/tax_class';
  public const DIVIDEBUY_GUEST_CHECKOUT_ENABLED = 'checkout/options/guest_checkout';
  public const DIVIDEBUY_POS_PRODUCT = 'dividebuy-pos-product';
  public const DEFAULT_EMAIL = 'example@example.com';
  public const DEFAULT_FIRST_NAME = 'First Name';
  public const DEFAULT_LAST_NAME = 'Last Name';
  public const DEFAULT_STREET = ['0' => 'Brindley Court', '1' => 'Lymedale Business Park'];
  public const DEFAULT_CITY = 'Staffordshire';
  public const DEFAULT_REGION = 'Staffordshire';
  public const DEFAULT_TELEPHONE = '08000850885';
  public const NON_DELIVERABLE_POSTCODE_MSG = 'Unfortunately we are unable to deliver to this postcode. Please call our Customer Service Team for more details: 0800 085 0885.';
}
