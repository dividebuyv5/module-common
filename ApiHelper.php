<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Dividebuy\Common\Logger\Logger;
use Dividebuy\Common\Utility\JsonHelper;
use Dividebuy\Common\Utility\RestClient;
use Dividebuy\Common\Utility\StoreConfigHelper;
use DivideBuySdk\Api\ApiFactory;
use DivideBuySdk\ApiInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Throwable;

class ApiHelper
{
  private Logger $sdkLogger;
  private LoggerInterface $logger;
  private RestClient $restClient;
  private JsonHelper $jsonHelper;
  private StoreConfigHelper $configHelper;
  private string $lastResponse = '';

  public function __construct(
      RestClient $restClient,
      StoreConfigHelper $configHelper,
      JsonHelper $jsonHelper,
      LoggerInterface $logger,
      Logger $sdkLogger
  ) {
    $this->logger = $logger;
    $this->sdkLogger = $sdkLogger;
    $this->restClient = $restClient;
    $this->jsonHelper = $jsonHelper;
    $this->configHelper = $configHelper;
  }

  public function getSplashRedirectUrl($storeId, $splashKey, $phoneOrderToken = ''): string
  {
    $redirectDomainPrefix = $this->configHelper->getOrderUrlDomainPrefix($storeId);
    $redirectEndpoint = $this->getSdkApi('', $storeId)->getUrlResolver()->getOrderDomain();
    $queryParams = array_filter(['splashKey' => $splashKey, 'token' => $phoneOrderToken]);
    $queryString = http_build_query($queryParams);

    return sprintf('https://%s.%s?%s', $redirectDomainPrefix, $redirectEndpoint, $queryString);
  }

  /**
   * @param ?string  $env
   * @param  int  $storeId
   *
   * @return ApiInterface
   */
  public function getSdkApiCurrentStore($env = '', $storeId = 0): ApiInterface
  {
    $storeId = $storeId ? $storeId : $this->configHelper->getCurrentStoreId();
    $env = $env ? $env : $this->configHelper->getEnvironment($storeId);
    $token = $this->configHelper->getStoreToken($storeId);
    $authenticationKey = $this->configHelper->getAuthenticationKey($storeId);

    return (new ApiFactory())->create($token, $authenticationKey, $env, $this->sdkLogger);
  }

  /**
   * @param ?string  $env
   * @param  int  $storeId
   *
   * @return ApiInterface
   */
  public function getSdkApi($env = '', $storeId = 0): ApiInterface
  {
    $storeId = $storeId ? $storeId : $this->configHelper->getStoreId();
    $env = $env ? $env : $this->configHelper->getEnvironment($storeId);
    $token = $this->configHelper->getStoreToken($storeId);
    $authenticationKey = $this->configHelper->getAuthenticationKey($storeId);

    return (new ApiFactory())->create($token, $authenticationKey, $env, $this->sdkLogger);
  }

  public function getSoftSearchDomain($storeId): string
  {
    return (string) $this->getSdkApi('', $storeId)->getUrlResolver()->getSoftSearchDomain();
  }

  public function getSoftSearchUrl($storeId): string
  {
    return (string) $this->getSdkApi('', $storeId)->getUrlResolver()->getSoftSearchDomain();
  }

  /**
   * Used to send CURL request.
   *
   * @param  string  $url
   * @param  array|string  $params
   * @param  string  $method
   *
   * @return array|mixed
   */
  public function sendRequest(string $url, $params = [], $method = 'POST')
  {
    $response = '';

    try {
      $response = $this->makeRequest($url, $params, $method);

      if ($this->configHelper->isLoggable()) {
        $dataResponse = '=========='.__METHOD__."==========\n";
        $dataResponse .= "Request: {$method} {$url}\n";
        $dataResponse .= 'Param :'.$this->encode($params)."\n";
        $dataResponse .= 'Response :'.$this->lastResponse."\n";

        $this->getLogger()->info($dataResponse);
      }
    } catch (Throwable $e) {
      $this->logger->critical($e);
    }

    return $response;
  }

  public function getPosApiEndpoint($storeId): string
  {
    $domain = $this->getSdkApi('', $storeId)->getUrlResolver()->getApiDomain();

    return sprintf('%sapi/pos/sendCheckoutUrlPos', $domain);
  }

  public function getPortalUrl($storeId): string
  {
    return (string) $this->getSdkApi('', $storeId)->getUrlResolver()->getPortalDomain();
  }

  public function getRestClient(): RestClient
  {
    return $this->restClient;
  }

  public function getLogger(): LoggerInterface
  {
    return $this->logger;
  }

  public function encode($data)
  {
    return $this->jsonHelper->encode($data);
  }

  public function decode($data)
  {
    return $this->jsonHelper->decode($data);
  }

  /**
   * @param  string  $url
   * @param  array  $params
   * @param  string  $method
   *
   * @return mixed
   *
   * @throws GuzzleException
   */
  private function makeRequest(string $url, $params = [], string $method = 'GET')
  {
    $apiResponse = $this->getRestClient()->request($url, $params, $method);
    $data = (string) $apiResponse->getBody();
    $this->lastResponse = $data;

    return $this->decode($data);
  }
}
