<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface RateInterface
{
  public function getRateId();

  public function getCode();

  public function getPrice();
}
