<?php

declare(strict_types=1);

namespace Dividebuy\Common\Exception;

use InvalidArgumentException;

class InvalidParameterException extends InvalidArgumentException
{
}
