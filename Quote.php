<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Quote\Model\Quote as MagentoQuote;

abstract class Quote extends MagentoQuote implements DivideBuyQuoteInterface
{
}
