<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Sales\Model\Order as MagentoOrder;

abstract class Order extends MagentoOrder implements DivideBuyOrderInterface
{
}
