<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Framework\Registry as MagentoRegistry;

class Registry extends MagentoRegistry
{
  /**
  * @var \Magento\Framework\Registry
  */
 
  protected $_registry;

   /**
   * ...
   * ...
   * @param \Magento\Framework\Registry $registry,
   */
   
  public function __construct(
      \Magento\Framework\Registry $registry
  ) {
      $this->_registry = $registry;
  }

  public function registry($key)
  {
    return $this->_registry->registry($key);
  }

  public function register($key, $value, $graceful = false)
  {
    return $this->_registry->register($key, $value, $graceful);
  }

  public function unregister($key)
  {
    return $this->_registry->unregister($key);
  }
}