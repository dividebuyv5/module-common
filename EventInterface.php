<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface EventInterface
{
  public function getTrack();
  public function getShipment();
  public function getOrder();
}
