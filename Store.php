<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Store\Model\Store as MagentoStore;

abstract class Store extends MagentoStore implements StoreInterface
{
}
