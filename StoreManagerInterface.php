<?php

declare(strict_types=1);

namespace Dividebuy\Common;

use Magento\Store\Model\StoreManagerInterface as MagentoStoreManagerInterface;

interface StoreManagerInterface extends MagentoStoreManagerInterface
{
  public function load($id, $scope = '');
}
