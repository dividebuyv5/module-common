<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface CustomerInterface
{
  public function getFirstname();
  public function getLastname();
  public function getEmail();
}
