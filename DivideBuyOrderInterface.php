<?php

declare(strict_types=1);

namespace Dividebuy\Common;

interface DivideBuyOrderInterface
{
  public function setHideDividebuy($data);

  public function getHideDividebuy();
  public function getDividebuySelectedInstalments();
  public function setDividebuySelectedInstalments($data);
}
